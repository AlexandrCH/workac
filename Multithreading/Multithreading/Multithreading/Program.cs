﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading
{
    class Program
    {
        static string fileName = "ReadTest.txt";

        static void Main(string[] args)
        {
            Console.WriteLine($"Main() invoked on thread #{Thread.CurrentThread.ManagedThreadId}.\n");

            ThreadsReadFile sm = new ThreadsReadFile();
            sm.ClearFile(fileName);
            sm.CheckFileDataInAnotherThread(fileName);
            sm.ReadFile(fileName);
            Console.ReadKey();
        }
    }
}
