﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //example #1
            Console.WriteLine("\nexample #1");
            DllReflection2 dr = new DllReflection2();
            dr.GetDataFromDll2();

            //example #2
            Console.WriteLine("\nexample #2");
            DllReflection dllReflection = new DllReflection();
            dllReflection.GetDataFromDll();
        }
    }
}
