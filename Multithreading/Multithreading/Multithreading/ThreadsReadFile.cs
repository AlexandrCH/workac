﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading
{
    delegate bool CheckingFileDelegate(string file);

    class ThreadsReadFile
    {
        //read file data
        public void ReadFile(string fileName)
        {
            Console.WriteLine($"ReadFile() invoked on thread {Thread.CurrentThread.ManagedThreadId}.");
            using (StreamReader sr = new StreamReader(fileName))
            {
                var temp = sr.ReadToEnd();
                Console.WriteLine("\nData from file:\n");
                Console.WriteLine(temp);
            }
        }

        //check file on any data every 10 seconds
        bool CheckFileSize(string file)
        {
            Console.WriteLine($"FileIsEmty() invoked on thread # {Thread.CurrentThread.ManagedThreadId}.\n");
            FileInfo fi = new FileInfo(file);
            while (fi.Length == 0)
            {
                Console.WriteLine($"File is empty! thread # {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(10000);
                fi = new FileInfo(file);
            }
            return true;
        }

        //data verification of the file in another thread using BeginInvoke() 
        public void CheckFileDataInAnotherThread(string fileName)
        {
            CheckingFileDelegate cfd = new CheckingFileDelegate(CheckFileSize);
            var ac = new AsyncCallback(CheckReady);
            IAsyncResult _iAsyncResult = cfd.BeginInvoke(fileName, ac, null);

            //Write message to main thread until action is running
            while (!_iAsyncResult.IsCompleted)
            {
                Thread.Sleep(1000);
                Console.WriteLine($"Working....on thread # {Thread.CurrentThread.ManagedThreadId}");
            }
            cfd.EndInvoke(_iAsyncResult);
        }

        //clear all data in the file
        public void ClearFile(string path)
        {
            File.WriteAllText(path, String.Empty);
        }

        //method will be called in delegate automaticly asncc call is finished
        void CheckReady(IAsyncResult _IAsyncResult)
        {
            Console.WriteLine($"File has information!!!! thread #{Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
