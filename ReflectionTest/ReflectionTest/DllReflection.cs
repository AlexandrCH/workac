﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTest
{
    class DllReflection
    {
        public void GetDataFromDll()
        {

            string pathToFile = @"D:\qqqqq\Education_dotNet_Reflection_classes.dll";
            string interfaceName = "IInterface";
            string propertyName = "CurrentIndex";
            string methodName = "GetNextIndex";
            var valueSetProperty = 45;

            //load assembly
            var fileDLL = Assembly.LoadFrom(pathToFile);

            List<object> listOfInstanses = new List<object>();

            //array of types from assembly
            Type[] types = fileDLL.GetTypes();

            #region Example with 'for' loop
            //for (int i = 0; i < types.Length; i++)
            //{
            //    Type type = fileDLL.GetType(types[i].FullName);
            //    if (type.GetInterface(interfaceName) != null)
            //    {
            //        object obj = Activator.CreateInstance(type);
            //        if (obj != null)
            //            actions.Add(obj);
            //    }
            //}
            #endregion


            foreach (var item in types)
            {
                //get type  name
                var type = fileDLL.GetType(item.FullName);

                //if type has "IInterface" creat it instance
                if (type.GetInterface(interfaceName) != null)
                {
                    //creating instanse
                    object obj = Activator.CreateInstance(type);
                    if (obj != null)
                    {
                        //add to list
                        listOfInstanses.Add(obj);
                    }
                }
            }

            foreach (var instanceClass in listOfInstanses)
            {
                foreach (Type _type in types.Where(x => x.GetInterface(interfaceName) != null))
                {
                    //get property by name
                    PropertyInfo prop = _type.GetProperty(propertyName);
                    Console.WriteLine("Property name:   " + prop.Name);

                    //set property value
                    prop.SetValue(instanceClass, valueSetProperty, null);
                    Console.WriteLine("Property value:   " + prop.GetValue(instanceClass));

                    //call 'GetNextIndex' method
                    MethodInfo mf = _type.GetMethod(methodName);
                    var returningValue = mf.Invoke(instanceClass, null);
                    Console.WriteLine("Method name:  " + mf.Name + "\nMethod value: " + returningValue);
                }
            }
            Console.ReadKey();
        }
    }
}
