﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilesStreams
{
    class DataHandler
    {
        //fields
        int count = 10;
        string fileName = @"test.txt";

        /// <summary>
        /// Data and Stream
        /// </summary>
        public void DataStream()
        {
            //Create stream in the memory using MemoryStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Console.WriteLine($"Enter {count} values! ");

                //Write data in MemoryStream
                for (int i = 1; i <= count; i++)
                {
                    Console.WriteLine($"Enter value #{i} and press 'Enter': ");
                    var temp = Console.ReadLine();
                    byte[] textInBytes = Encoding.Default.GetBytes(temp);
                    memoryStream.Write(textInBytes, 0, textInBytes.Length);
                }

                //Write data from MemoryStream in FileStream (file)
                using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    memoryStream.WriteTo(fileStream);
                }
            }

            //Open new FileStream only for reading
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                byte[] bytesArray = new byte[fs.Length];
                int maxNumBytesToRead = (int)fs.Length;
                fs.Read(bytesArray, 0, maxNumBytesToRead);
                var dataFromFile = Encoding.Default.GetString(bytesArray);

                Console.WriteLine($"\nDisplay all data from '{fileName}' file:\n" + dataFromFile);
            }
            Console.ReadKey();
        }
    }
}
