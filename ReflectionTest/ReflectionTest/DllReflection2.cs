﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTest
{
    class DllReflection2
    {
        public void GetDataFromDll2()
        {
            string debugLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string pathToFile = @"D:\qqqqq\Education_dotNet_Reflection_classes.dll";
            string interfaceName = "IInterface";
            string propertyName = "CurrentIndex";
            string methodName = "GetNextIndex";
            string tempType = "";
            var valueSetProperty = 15;

            //load assembly
            var fileDLL = Assembly.LoadFrom(pathToFile);

            //get type that has "IInterface"
            var result = fileDLL.GetTypes().Where(x => x.GetInterface(interfaceName) != null);

            foreach (var item in result)
            {
                //get name of Type that has "IInterface"
                tempType = item.FullName.ToString();

                //get type that has "IInterface" by FullName
                Type _type = fileDLL.GetType(tempType);

                //create instance
                object instanceClass = Activator.CreateInstance(_type);
                Console.WriteLine("Current object:  " + instanceClass.GetType().ToString());

                //get property by name
                PropertyInfo prop = _type.GetProperty(propertyName);
                Console.WriteLine("Property name:   " + prop.Name);

                //set property value
                prop.SetValue(instanceClass, valueSetProperty, null);
                Console.WriteLine("Property value:   " + prop.GetValue(instanceClass));

                //call 'GetNextIndex' method
                MethodInfo mf = _type.GetMethod(methodName);
                var returningValue = mf.Invoke(instanceClass, null);
                Console.WriteLine("Method name:  " + mf.Name + "    Method value:   " + returningValue);
            }
        }
    }
}
