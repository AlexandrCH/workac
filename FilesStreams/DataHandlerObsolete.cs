﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilesStreams
{
    class DataHandlerObsolete
    {
        int count = 10;
        string fileName = @"test.txt";

        [Obsolete]
        public void DataStream()
        {
            //create stream in the memory
            using (MemoryStream memoryStream = new MemoryStream())
            {
                //write data in the memory stream
                using (StreamWriter streamWriter = new StreamWriter(memoryStream))
                {
                    Console.WriteLine("Enter 10 values! ");

                    for (int i = 1; i <= count; i++)
                    {
                        Console.WriteLine($"Enter value #{i} and press 'Enter': ");
                        var temp = Console.ReadLine();
                        streamWriter.WriteLine(temp);
                    }

                    streamWriter.Flush();

                    // write data in the file
                    using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                    {
                        memoryStream.WriteTo(fileStream);
                    }
                }
            }

            //read data from file
            using (StreamReader streamReader = new StreamReader(fileName))
            {
                Console.WriteLine("\nDisplay all data from file:\n");

                var read = streamReader.ReadToEnd();
                Console.WriteLine(read);
            }

            Console.ReadKey();
        }
    }
}
