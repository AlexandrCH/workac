﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education_dotNet_Serialization_class;
using System.Xml.Serialization;
using System.IO;

namespace SerializationTest
{
    class ClassSerialization
    {
        static string fileName = "testSerialization2.xml";

        public static void Serialize()
        {

            object instanceOrder = Activator.CreateInstance(typeof(Order));

            //XmlAttributeOverrides xmlOverAtribute = new XmlAttributeOverrides();
            //XmlAttributes xmlAttr = new XmlAttributes();
            //xmlAttr.XmlIgnore = true;
            //xmlOverAtribute.Add(typeof(Order), "TotalPrice", xmlAttr);

            //XmlSerializer xmlFormat = new XmlSerializer(typeof(Order), xmlOverAtribute);

            XmlSerializer xmlFormat = new XmlSerializer(typeof(Order));
            using (Stream fStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlFormat.Serialize(fStream, instanceOrder);
            }
            Console.WriteLine("\n==== Saved Order in XML format!");

            XmlSerializer _xmlFormat = new XmlSerializer(typeof(Order));
            using (Stream reader = new FileStream(fileName, FileMode.Open))
            {
                // Call the Deserialize method to restore the object's state.
                var d = (Order)_xmlFormat.Deserialize(reader);
                Console.WriteLine("Count:  " + d.Count);
                Console.WriteLine("Price:  " + d.Price);
                Console.WriteLine("TotalPrice:  " + d.GetTotalPrice);
            }
        }
    }
}
