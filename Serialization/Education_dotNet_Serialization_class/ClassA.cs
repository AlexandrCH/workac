﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Education_dotNet_Serialization_class
{
    [Serializable]
    public class Order
    {
        public Order(){}

        public int Count = 89;

        public decimal Price = 47;
        [NonSerialized]
        decimal TotalPrice = 13;

        public decimal GetTotalPrice
        {
            get { return TotalPrice; }
        }

        public Order(int count, decimal price)
        {
            this.Count = count;
            this.Price = price;

            this.TotalPrice = count * price;
        }
    }
}