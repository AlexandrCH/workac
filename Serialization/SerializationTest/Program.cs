﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education_dotNet_Serialization_class;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;

namespace SerializationTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassSerialization.Serialize();
            SoapSerializeClass();
            XMLSerializeClass();
            BinarySerializeClass();
        }

        static void XMLSerializeClass()
        {
            string fileNameXML = "testSerialization.xml";

            Order order = new Order(800, 200);
            ////making ignore atribute 
            //XmlAttributeOverrides xmlOverAtribute = new XmlAttributeOverrides();
            //XmlAttributes xmlAttr = new XmlAttributes();
            //xmlAttr.XmlIgnore = true;
            //xmlOverAtribute.Add(typeof(Order), "TotalPrice", xmlAttr);
            ////XmlSerializer with ignore atribute
            //XmlSerializer xmlFormat = new XmlSerializer(typeof(Order), xmlOverAtribute);
            XmlSerializer xmlFormat = new XmlSerializer(typeof(Order));
            using (Stream fStream = new FileStream(fileNameXML, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlFormat.Serialize(fStream, order);
            }
            Console.WriteLine("\n==== Saved Order in XML format!");
            //order = null;
            //desirialization
            XmlSerializer _xmlFormat = new XmlSerializer(typeof(Order));
            using (Stream reader = new FileStream(fileNameXML, FileMode.Open))
            {
                // Call the Deserialize method 
                var d = (Order)_xmlFormat.Deserialize(reader);
                Console.WriteLine("Count:  " + d.Count);
                Console.WriteLine("Price:  " + d.Price);
                Console.WriteLine("TotalPrice:  " + d.GetTotalPrice);
            }
        }
        static void BinarySerializeClass()
        {
            string fileNameBinary = "testSerialization.dat";

            Order order = new Order(500, 200);

            BinaryFormatter binFormat = new BinaryFormatter();
            //serialization to BinaryFormatter
            using (Stream fStream = new FileStream(fileNameBinary, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, order);
            }
            Console.WriteLine("\n==== Saved Order in Binary format!");

            BinaryFormatter binFormat2 = new BinaryFormatter();
            using (Stream fStream = File.OpenRead(fileNameBinary))
            {
                Order _order = (Order)binFormat2.Deserialize(fStream);
                Console.WriteLine("Count:  " + _order.Count);
                Console.WriteLine("Price:  " + _order.Price);
                Console.WriteLine("TotalPrice:  " + _order.GetTotalPrice);
            }
        }

        static void SoapSerializeClass()
        {
            string fileNameSoap = "testSerialization.soap";
            Order order3 = new Order(450, 300);

            XmlAttributeOverrides xmlOverAtribute = new XmlAttributeOverrides();
            XmlAttributes xmlAttr = new XmlAttributes();
            xmlAttr.XmlIgnore = true;
            xmlOverAtribute.Add(typeof(Order), "TotalPrice", xmlAttr);

            SoapFormatter soapFormat = new SoapFormatter();
            using (Stream fStream = new FileStream(fileNameSoap, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                soapFormat.Serialize(fStream, order3);
            }
            Console.WriteLine("\n==== Saved Order in SOAP format");
            SoapFormatter soapFormat2 = new SoapFormatter();

            using (Stream fStream = File.OpenRead(fileNameSoap))
            {
                Order _order = (Order)soapFormat2.Deserialize(fStream);
                Console.WriteLine("Count:  " + _order.Count);
                Console.WriteLine("Price:  " + _order.Price);
                Console.WriteLine("TotalPrice:  " + _order.GetTotalPrice);
            }
        }
    }
}
